
var autoscroll = true;
var current = 1;

var frameSettings = {
    "0": { "offset": 0 },
    "1": { "offset": 0 },
    "2": { "offset": 10 },
    "3": { "offset": 15 },
    "4": { "offset": 15 },
    "5": { "offset": 180 },
    "6": { "offset": 280 },
    "7": { "offset": 0 },
    "8": { "offset": 35 },
    "9": { "offset": -25 }
};


$(function () {

    $("#replay").click(function () {
        resetAll();
        frame1();
        clearFrame9Intervals();
        current = 1;
    });
    $("#scrollUp").click(function () {
        if (current != 1) {
            current = current - 1;
            scrollToFrame(".frame" + current, frameSettings[current].offset);
            eval("frame" + current + "Reset")();
            eval("frame" + current)();
        }
    });
    $("#scrollDown").click(function () {
        if (current != 9) {
            current = current + 1;
            scrollToFrame(".frame" + current, frameSettings[current].offset);
            eval("frame" + current + "Reset")();
            eval("frame" + current)();
        }
    });

    frame1();

    var frame1_start, frame1_text2, frame1_text3, frame1_text4, frame1_text5, frame1_end, frame1_title_fadein
    function frame1() {
        frame1Reset();
        $("#frame1-title").fadeIn("slow", function () {
            frame1_start = setInterval(function () {
                clearInterval(frame1_start);
                $("#frame1-title").fadeOut("slow", function () {

                    scrollToFrame('#frame1-dee', 150);
                    $("#frame1-text1").fadeIn("slow");
                });

                frame1_text2 = setInterval(function () {
                    clearInterval(frame1_text2);
                    $("#frame1-text1").fadeOut("slow", function () {
                        $("#frame1-text2").fadeIn("slow");
                    });

                    frame1_text3 = setInterval(function () {
                        clearInterval(frame1_text3);
                        $("#frame1-text2").fadeOut("slow", function () {
                            $("#frame1-text3").fadeIn("slow");
                        });

                        frame1_text4 = setInterval(function () {
                            clearInterval(frame1_text4);
                            $("#frame1-text3").fadeOut("slow", function () {
                                $("#frame1-text4").fadeIn("slow");
                            });

                            frame1_text5 = setInterval(function () {
                                clearInterval(frame1_text5);
                                $("#frame1-text4").fadeOut("slow", function () {
                                    $("#frame1-text5").fadeIn("slow");
                                });

                                frame1_title_fadein = setInterval(function () {
                                    clearInterval(frame1_title_fadein);
                                    $("#frame1-text5").fadeOut("slow");
                                    $("#frame1-title").fadeIn("slow");
                                }, 20000);

                            }, 3000);

                        }, 3000);

                    }, 3000);

                }, 3000);

            }, 2000);
        });
    }
    function frame1Reset() {
        clearInterval(frame1_start);
        clearInterval(frame1_text2);
        clearInterval(frame1_text3);
        clearInterval(frame1_text4);
        clearInterval(frame1_text5);
        clearInterval(frame1_end);
        clearInterval(frame1_title_fadein);
        $("#frame1-text1").hide();
        $("#frame1-text2").hide();
        $("#frame1-text3").hide();
        $("#frame1-text4").hide();
        $("#frame1-text5").hide();
        $("#frame1-title").fadeIn("slow");
    }

    var frame2_start, frame2_text, frame2_end
    function frame2() {
        frame2Reset();
        frame2_start = setInterval(function () {
            clearInterval(frame2_start);

            $("#frame2-image1").attr('src', 'assets/img/gem-foreground.gif');
            $("#frame2-image2").attr('src', 'assets/img/dancing_dee.gif');
            $("#frame2-image3").attr('src', 'assets/img/gem-background.gif');

            $("#frame2-image1").fadeIn("slow");
            $("#frame2-image2").fadeIn("slow");
            $("#frame2-image3").fadeIn("slow");

            frame2_text = setInterval(function () {
                clearInterval(frame2_text);
                $("#frame2-text").fadeIn("slow");

            }, 1000);

        }, 2000);
    }
    function frame2Reset() {
        clearInterval(frame2_start);
        clearInterval(frame2_text);
        clearInterval(frame2_end);
        $("#frame2-image1").hide();
        $("#frame2-image2").hide();
        $("#frame2-image3").hide();
        $("#frame2-text").hide();
    }

    var frame3_start, frame3_text, frame3_end
    function frame3() {
        frame3Reset();
        frame3_start = setInterval(function () {
            clearInterval(frame3_start);

            $("#frame3-image1").attr('src', 'assets/img/parrot_foreground.gif');
            $("#frame3-image2").attr('src', 'assets/img/dancing_dee_blue.gif');

            $("#frame3-image1").fadeIn("slow");
            $("#frame3-image2").fadeIn("slow");

            frame3_text = setInterval(function () {
                clearInterval(frame3_text);
                $("#frame3-text").fadeIn("slow");
            }, 1000);

        }, 2000);
    }
    function frame3Reset() {
        clearInterval(frame3_start);
        clearInterval(frame3_text);
        clearInterval(frame3_end);
        $("#frame3-image1").hide();
        $("#frame3-image2").hide();
        $("#frame3-text").hide();
    }

    var frame4_start, frame4_text, frame4_end
    function frame4() {
        frame4Reset();
        frame4_start = setInterval(function () {
            clearInterval(frame4_start);

            $("#frame4-image1").attr('src', 'assets/img/under-sea-dee.gif');
            $("#frame4-image1").fadeIn("slow");

            frame4_text = setInterval(function () {
                clearInterval(frame4_text);
                $("#frame4-text").fadeIn("slow");
            }, 1000);

        }, 2000);
    }
    function frame4Reset() {
        clearInterval(frame4_start);
        clearInterval(frame4_text);
        clearInterval(frame4_end);
        $("#frame4-image1").hide();
        $("#frame4-text").hide();
    }

    var frame5_start, frame5_end
    function frame5() {
        frame5Reset();
        frame5_start = setInterval(function () {
            clearInterval(frame5_start);
            $("#frame5-text").fadeIn("slow");
        }, 1000);
    }
    function frame5Reset() {
        clearInterval(frame5_start);
        clearInterval(frame5_end);
        $("#frame5-text").hide();
    }

    var frame6_start, frame6_text, frame6_end
    function frame6() {
        frame6Reset();
        frame6_start = setInterval(function () {
            clearInterval(frame6_start);

            $("#frame6-image1").attr('src', 'assets/img/tumbling-dee.gif');
            $("#frame6-image1").fadeIn("slow");

            frame6_text = setInterval(function () {
                clearInterval(frame6_text);
                $("#frame6-text1").fadeIn("slow", function () {
                    $("#frame6-text2").fadeIn("slow", function () {
                        $("#frame6-text3").fadeIn("slow", function () {
                            $("#frame6-text4").fadeIn("slow", function () {

                            });
                        });
                    });
                });
            }, 1000);
        }, 2000);
    }
    function frame6Reset() {
        clearInterval(frame6_start);
        clearInterval(frame6_text);
        clearInterval(frame6_end);
        $("#frame6-image1").hide();
        $("#frame6-text1").hide();
        $("#frame6-text2").hide();
        $("#frame6-text3").hide();
        $("#frame6-text4").hide();
    }

    var frame7_start, frame7_text1, frame7_text2, frame7_end
    function frame7() {
        frame7Reset();
        frame7_start = setInterval(function () {

            clearInterval(frame7_start);
            $("#frame7-image1").attr('src', 'assets/img/idea-dee1.gif');
            $("#frame7-image1").fadeIn("slow");
            $("#frame7-text1").fadeIn("slow", function () {

                frame7_text1 = setInterval(function () {
                    clearInterval(frame7_text1);
                    $("#frame7-text1").fadeOut("slow", function () {
                        $("#frame7-text2").fadeIn("slow", function () {

                            frame7_text2 = setInterval(function () {
                                clearInterval(frame7_text2);
                                $("#frame7-text1").fadeOut("slow");
                                $("#frame7-text2").fadeOut("slow", function () {
                                    $("#frame7-image2").attr('src', 'assets/img/idea-dee2.gif');
                                    $("#frame7-image2").fadeIn("slow");
                                    $("#frame7-text3").fadeIn("slow");
                                });
                            }, 3000);
                        });
                    });

                }, 3000);

            });

        }, 1000);
    }
    function frame7Reset() {
        clearInterval(frame7_start);
        clearInterval(frame7_text1);
        clearInterval(frame7_text2);
        clearInterval(frame7_end);
        $("#frame7-image1").hide();
        $("#frame7-image2").hide();
        $("#frame7-text1").hide();
        $("#frame7-text2").hide();
        $("#frame7-text3").hide();
    }

    var frame8_start, frame8_end
    function frame8() {
        frame8Reset();
        frame8_start = setInterval(function () {
            clearInterval(frame8_start);

            $("#frame8-image1").attr('src', 'assets/img/cwa-dee.gif');
            $("#frame8-image1").fadeIn("slow");

            $("#frame8-text1").fadeIn("slow", function () {
                $("#frame8-text2").fadeIn("slow");
                $("#frame8-text3").fadeIn("slow");
                $("#frame8-text4").fadeIn("slow");
            });

        }, 1000);
    }
    function frame8Reset() {
        clearInterval(frame8_start);
        clearInterval(frame8_end);
        $("#frame8-image1").hide();
        $("#frame8-image2").hide();
        $("#frame8-text1").hide();
        $("#frame8-text2").hide();
        $("#frame8-text3").hide();
        $("#frame8-text4").hide();
    }

    var frame9_start, frame9_end, visit_text;
    function frame9() {
        frame9Reset();
        frame9_start = setInterval(function () {
            clearInterval(frame9_start);

            $("#frame9-image1").attr('src', 'assets/img/happy-holidays-dee.gif');
            $("#frame9-image1").fadeIn("slow", function () {

                frame9_end = setInterval(function () {
                    clearInterval(frame9_end);
                    frame9_ended = true;
                    visit_text = setInterval(function () {
                        clearInterval(visit_text);
                        $("#visit").fadeIn("slow");
                    }, 5000);
                }, 2000);

            });

        }, 2000);
    }
    function frame9Reset() {
        clearInterval(frame9_start);
        clearInterval(frame9_end);
        clearInterval(visit_text);
        $("#frame9-image1").hide();
        $("#frame9-text").hide();
    }
    function clearFrame9Intervals() {
        clearInterval(frame9_start);
        clearInterval(frame9_end);
        clearInterval(visit_text);
    }

    function resetAll() {
        frame1_ended = false;
        frame2_ended = false;
        frame3_ended = false;
        frame4_ended = false;
        frame5_ended = false;
        frame6_ended = false;
        frame7_ended = false;
        frame8_ended = false;
        frame9_ended = false;
        frame1Reset();
        frame2Reset();
        frame3Reset();
        frame4Reset();
        frame5Reset();
        frame6Reset();
        frame7Reset();
        frame8Reset();
        frame9Reset();
        scrollToFrame('.frame1', 0);
    }

    function scrollToFrame(name, offset) {
        var vh = $(window).width();
        if (vh >= 768 && vh < 1000) { offset = offset + 150; }
        else if (vh >= 375 && vh < 768) { offset = offset + 30; }
        else { offset = offset + 100; }
        $('html, body').animate({
            scrollTop: $(name).offset().top - offset
        }, 3500);
    }

});